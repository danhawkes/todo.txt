module.exports = {
  rollup(config, options) {
    config.external = ["chevrotain"];
    config.output.globals = { chevrotain: "chevrotain" };
    return config;
  }
};
