import * as tokens from "./tokens";
import { Lexer } from "chevrotain";

export default class ChevLexer extends Lexer {
  constructor() {
    super(tokens.allTokens);
  }
}
