import * as tokens from "./tokens";
import { CstParser } from "chevrotain";

export default class ChevParser extends CstParser {
  constructor() {
    super(tokens.allTokens);
    this.performSelfAnalysis();
  }
  private checkmark = this.RULE("checkmark", () =>
    this.OPTION1(() => this.CONSUME(tokens.checkmark))
  );
  private priority = this.RULE("priority", () =>
    this.OPTION2(() => this.CONSUME(tokens.priority))
  );
  private dates = this.RULE("dates", () =>
    this.OPTION3(() => {
      this.CONSUME1(tokens.date);
      this.OPTION4(() => this.CONSUME2(tokens.date));
    })
  );
  private body = this.RULE("body", () =>
    this.AT_LEAST_ONE(() => {
      this.OR([
        { ALT: () => this.CONSUME1(tokens.project) },
        { ALT: () => this.CONSUME2(tokens.context) },
        { ALT: () => this.CONSUME3(tokens.extra) },
        { ALT: () => this.CONSUME4(tokens.word) }
      ]);
    })
  );
  private todoItem = this.RULE("todoItem", () => {
    this.SUBRULE1(this.checkmark);
    this.SUBRULE2(this.priority);
    this.SUBRULE3(this.dates);
    this.SUBRULE4(this.body);
  });
  public todoList = this.RULE("todoList", () => {
    this.OPTION1(() => this.SUBRULE1(this.todoItem));
    this.MANY1(() => {
      this.AT_LEAST_ONE(() => this.CONSUME1(tokens.newline));
      this.SUBRULE2(this.todoItem);
    });
    this.MANY2(() => this.CONSUME2(tokens.newline));
  });
}
