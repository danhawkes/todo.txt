import ToDo from "./ToDo";

import ToDoChevLexer from "./ChevLexer";
import ChevParser from "./ChevParser";
import newVisitor from "./ChevVisitor";
import { ICstVisitor, IRecognitionException } from "chevrotain";

export class ToDoParserError extends Error {
  constructor(public readonly causes: IRecognitionException[]) {
    super(causes.toString());
  }
}
export default class ToDoParser {
  public lexer: ToDoChevLexer;
  public parser: ChevParser;
  visitor: ICstVisitor<any, any>;

  constructor() {
    this.parser = new ChevParser();
    this.lexer = new ToDoChevLexer();
    this.visitor = newVisitor(this.parser);
  }

  parse(input: string): ToDo[] {
    let lexingResult = this.lexer.tokenize(input);
    this.parser.reset();
    this.parser.input = lexingResult.tokens;
    let cst = this.parser.todoList();
    if (this.parser.errors.length > 0) {
      throw new ToDoParserError(this.parser.errors);
    }

    return this.visitor.visit(cst);
  }
}
