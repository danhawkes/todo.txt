import {
  createToken,
  Lexer,
  IToken,
  tokenMatcher,
  TokenType
} from "chevrotain";

type KeyValuePayload = { key: string; value: string };
type DatePayload = Date;
type StringPayload = string;

export type PayloadCarrier<T> = RegExpExecArray & {
  payload: T;
};
export type PriorityToken = PayloadCarrier<StringPayload>;
export type DateToken = PayloadCarrier<DatePayload>;
export type ExtraToken = PayloadCarrier<KeyValuePayload>;
export type ProjectToken = PayloadCarrier<StringPayload>;
export type ContextToken = PayloadCarrier<StringPayload>;
export type WordToken = IToken;

function regexGroupMatcher<T extends PayloadCarrier<any>>(
  r: RegExp,
  f: (match: T) => void
) {
  return (text: string, offset: number): T | null => {
    return extractMatches(text, offset, r, f);
  };
}

function isType(token: IToken | undefined, tokType: TokenType) {
  return token ? tokenMatcher(token, tokType) : false;
}

function extractMatches<T extends PayloadCarrier<any>>(
  text: string,
  offset: number,
  r: RegExp,
  extract: (match: T) => void
) {
  r.lastIndex = offset;
  let matches = r.exec(text) as T;
  if (matches !== null) {
    extract(matches);
    return matches;
  }
  return null;
}

export const word = createToken({
  name: "Word",
  label: "word",
  line_breaks: false,
  pattern: /\S+/
});

export const date = createToken({
  name: "Date",
  label: "2020-01-01",
  line_breaks: false,
  longer_alt: word,
  pattern: (text, offset, tokens) => {
    let n1 = tokens.length >= 1 ? tokens[tokens.length - 1] : undefined;
    let n2 = tokens.length >= 2 ? tokens[tokens.length - 2] : undefined;
    let n1IsUndefined = n1 === undefined;
    let n1IsPriority = isType(n1, priority);
    let n1IsCheckmark = isType(n1, checkmark);
    let n1IsNewline = isType(n1, newline);
    let n1IsDate = isType(n1, date);
    let n2IsDate = isType(n2, date);
    if (
      n1IsUndefined ||
      n1IsCheckmark ||
      n1IsPriority ||
      n1IsNewline ||
      (n1IsDate && !n2IsDate)
    ) {
      return extractMatches(
        text,
        offset,
        /([0-9]{4}-[0-9]{2}-[0-9]{2})/y,
        matches => {
          matches.payload = new Date(matches[1]);
        }
      );
    }
    return null;
  }
});

export const project = createToken({
  name: "Project",
  label: "+project",
  line_breaks: false,
  pattern: regexGroupMatcher<ProjectToken>(/\+(\S+)/y, matches => {
    matches.payload = matches[1];
  })
});

export const context = createToken({
  name: "Context",
  label: "@context",
  line_breaks: false,
  pattern: regexGroupMatcher<ContextToken>(/@(\S+)/y, matches => {
    matches.payload = matches[1];
  })
});

export const extra = createToken({
  name: "Extra",
  label: "key:value",
  line_breaks: false,
  pattern: regexGroupMatcher<ExtraToken>(/(\S+):(\S+)/y, matches => {
    matches.payload = { key: matches[1], value: matches[2] };
  })
});

export const priority = createToken({
  name: "Priority",
  label: "(A)",
  line_breaks: false,
  longer_alt: word,
  pattern: regexGroupMatcher<PriorityToken>(/\(([A-Z])\)/y, matches => {
    matches.payload = matches[1];
  })
});

export const newline = createToken({
  name: "Newline",
  label: "EOL",
  pattern: /\r?\n/
});

export const checkmark = createToken({
  name: "Checkmark",
  label: "x",
  line_breaks: false,
  longer_alt: word,
  pattern: (text, offset, tokens) => {
    let previousToken = tokens.slice(-1)[0];
    if (previousToken === undefined || tokenMatcher(previousToken, newline)) {
      return extractMatches(text, offset, /x/y, matches => matches);
    }
    return null;
  }
});

export const whitespace = createToken({
  name: "Whitespace",
  pattern: /[ \t]+/,
  group: Lexer.SKIPPED
});

export const allTokens = [
  whitespace,
  newline,
  date,
  priority,
  checkmark,
  project,
  context,
  extra,
  word
];
