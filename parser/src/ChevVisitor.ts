import { CstParser, ICstVisitor } from "chevrotain";
import ToDo, { Extras } from "ToDo";
import {
  WordToken,
  PriorityToken,
  DateToken,
  ExtraToken,
  ContextToken,
  ProjectToken
} from "tokens";

export default function newVisitor(parser: CstParser): ICstVisitor<any, any> {
  class ChevVisitor extends parser.getBaseCstVisitorConstructor() {
    constructor() {
      super();
      this.validateVisitor();
    }
    checkmark(node: any) {
      return node.Checkmark !== undefined;
    }
    priority(node: any) {
      return (node.Priority as PriorityToken[])?.[0].payload ?? null;
    }
    dates(node: any) {
      let [first, second] = [
        node?.Date?.[0]?.payload ?? null,
        node?.Date?.[1]?.payload ?? null
      ] as (DateToken | null)[];
      return { dateCompleted: first, dateCreated: second };
    }
    body(node: any): any {
      let text = (node.Word as WordToken[]).map(w => w.image).join(" ");
      let projects =
        (node.Project as ProjectToken[])?.map(p => p.payload) ?? [];
      let contexts =
        (node.Context as ContextToken[])?.map(c => c.payload) ?? [];
      let extras =
        (node.Extra as ExtraToken[])
          ?.map(e => e.payload)
          .reduce((extras, token) => {
            extras[token.key] = token.value;
            return extras;
          }, {} as Extras) ?? {};
      return { text, projects, contexts, extras };
    }
    todoItem(node: any): ToDo {
      let body = this.visit(node.body);
      let dates = this.visit(node.dates);
      return {
        completed: this.visit(node.checkmark),
        priority: this.visit(node.priority),
        ...body,
        ...dates
      };
    }
    todoList(node: any): ToDo[] {
      let list = node.todoItem?.map((n: any) => this.visit(n)) ?? [];
      return list;
    }
  }
  return new ChevVisitor();
}
