export interface Extras {
  [key: string]: string;
}

export default interface ToDo {
  completed: boolean;
  priority: string | null;
  dateCompleted: Date | null;
  dateCreated: Date | null;
  text: string;
  projects: string[];
  contexts: string[];
  extras: Extras;
}
