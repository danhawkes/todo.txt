import { TokenType, ILexingResult } from "chevrotain";
import ToDoParser from "../src/ToDoParser";
import ChevLexer from "../src/ChevLexer";
import * as t from "../src/tokens";

expect.extend({
  toHaveTokenTypes(received: ILexingResult, expected: TokenType[]) {
    let n1 = received.tokens.map(token => token.tokenType.name);
    let n2 = expected.map(token => token.name);
    return {
      pass: JSON.stringify(n1) === JSON.stringify(n2),
      message: () => `Expected tokens:\n[${n2.join(", ")}]\n[${n1.join(", ")}]`
    };
  }
});

declare global {
  namespace jest {
    interface Matchers<R> {
      toHaveTokenTypes(token: TokenType[]): R;
    }
  }
}

const checkmark = "x";
const priority = "(A)";
const date2 = "2020-04-10";
const date1 = "2020-01-01";
const dates = `${date2} ${date1}`;
const word = "test";
const text = "write parser for todo.txt";
const project1 = "+code";
const projects = project1;
const context1 = "@typescript";
const context2 = "@js";
const contexts = `${context1} ${context2}`;
const extra1 = "due:2021-01-01";
const extra2 = "test:true";
const extras = `${extra1} ${extra2}`;
const newline = "\n";

let lexer: ChevLexer;
let parser: ToDoParser;

describe("Lexer", () => {
  function lex(input: string) {
    return lexer.tokenize(input);
  }
  beforeAll(() => {
    lexer = new ChevLexer();
  });
  test("newline", () => {
    let input = `${word}${newline}${word}${newline}`;
    expect(lex(input)).toHaveTokenTypes([t.word, t.newline, t.word, t.newline]);
  });
  test("checkmark", () => {
    expect(lex(`${checkmark} ${checkmark}`)).toHaveTokenTypes([
      t.checkmark,
      t.word
    ]);
    expect(lex(`${checkmark} ${newline} ${checkmark}`)).toHaveTokenTypes([
      t.checkmark,
      t.newline,
      t.checkmark
    ]);
    expect(lex(`${checkmark}x`)).toHaveTokenTypes([t.word]);
  });
  test("priority", () => {
    expect(lex(`${priority})`)).toHaveTokenTypes([t.word]);
  });
  test("date", () => {
    expect(lex("2020-01-01 2020-01-01 2020-01-01")).toHaveTokenTypes([
      t.date,
      t.date,
      t.word
    ]);
    expect(lex("x 2020-01-01")).toHaveTokenTypes([t.checkmark, t.date]);
    expect(lex("(A) 2020-01-01")).toHaveTokenTypes([t.priority, t.date]);
    expect(lex(`${newline} 2020-01-01`)).toHaveTokenTypes([t.newline, t.date]);
    expect(lex(`${date1}1`)).toHaveTokenTypes([t.word]);
  });
  test("comprehensive", () => {
    let input = `${checkmark} ${priority} ${dates} ${text} ${projects} ${contexts} ${extras}`;
    expect(lex(input)).toHaveTokenTypes([
      t.checkmark,
      t.priority,
      t.date,
      t.date,
      t.word,
      t.word,
      t.word,
      t.word,
      t.project,
      t.context,
      t.context,
      t.extra,
      t.extra
    ]);
  });
});

describe("Parser", () => {
  function parse(input: string) {
    return parser.parse(input);
  }
  beforeAll(() => {
    parser = new ToDoParser();
  });
  test("minimal", () => {
    let todo = parse(`${text}${newline}`)[0];
    expect(todo.completed).toEqual(false);
    expect(todo.priority).toBeNull();
    expect(todo.dateCreated).toBeNull();
    expect(todo.dateCompleted).toBeNull();
    expect(todo.text).toEqual(text);
    expect(todo.contexts).toEqual(expect.arrayContaining([]));
    expect(todo.projects).toEqual(expect.arrayContaining([]));
    expect(todo.extras).toEqual({});
  });
  test("comprehensive", () => {
    let todo = parse(
      `${checkmark} ${priority} ${dates} ${text} ${projects} ${extras} ${contexts}${newline}`
    )[0];
    expect(todo.completed).toEqual(true);
    expect(todo.priority).toEqual("A");
    expect(todo.dateCreated).toEqual(new Date("2020-01-01"));
    expect(todo.dateCompleted).toEqual(new Date("2020-04-10"));
    expect(todo.text).toEqual(text);
    expect(todo.contexts).toEqual(expect.arrayContaining(["typescript", "js"]));
    expect(todo.projects).toEqual(expect.arrayContaining(["code"]));
    expect(todo.extras).toEqual({ due: "2021-01-01", test: "true" });
  });

  test("single date", () => {
    let todo = parse(`${date2} ${text}${newline}`)[0];
    expect(todo.dateCreated).toBeNull();
    expect(todo.dateCompleted).toEqual(new Date("2020-04-10"));
  });
  test("empty", () => {
    let todos = parse("");
    expect(todos.length).toEqual(0);
  });
  test("no terminating newline", () => {
    let todos = parse(`${text}`);
    expect(todos.length).toEqual(1);
  });
  test("multiline", () => {
    let todos = parse(`${text}${newline}`.repeat(3));
    expect(todos.length).toEqual(3);
  });
});
