#!/bin/sh
set -e

case "$1" in
  clean)
    git clean -X -d -f;;
  lint)
    (cd parser && npm ci && npm run lint) && \
    (cd website && npm ci && npm run lint);;
  build)
    (cd parser && npm ci && npm run build && npm pack && mv ./*.tgz ../) && \
    (cd website && npm ci && npm run build && mv dist ../public);;
  test)
    (cd parser && npm ci && npm run test);;
  publish)
    echo "$NPM_RC" > ~/.npmrc
    npm publish ./*.tgz --access public;;
esac
