export default {
  target: "static",
  head: {
    title: "todo.txt parser",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description" }
    ]
  },
  router: { linkActiveClass: "is-active", base: "/todo.txt/" },
  loading: { color: "#fff" },
  css: ["~/assets/css/main.scss"],
  plugins: ["~/plugins/highlight"],
  buildModules: ["@nuxt/typescript-build"]
};
