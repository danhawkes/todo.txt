import hljs from "highlight.js/lib/core";
import javascript from "highlight.js/lib/languages/javascript";
import Vue from "vue";

hljs.registerLanguage("javascript", javascript);

function _highlight(el, binding) {
  const targets = el.querySelectorAll("code");
  Array.from(targets).forEach(target => {
    if (typeof binding.value === "string") {
      target.textContent = binding.value;
    }
    hljs.highlightBlock(target);
  });
}

function install(Vue) {
  Vue.directive("highlightjs", {
    deep: true,
    bind: function bind(el, binding) {
      _highlight(el, binding);
    },
    componentUpdated: function componentUpdated(el, binding) {
      _highlight(el, binding);
    }
  });
}

const vueHighlightJS = {
  install
};

Vue.use(vueHighlightJS);
